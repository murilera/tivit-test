# Given a number, try to return the following: "123...n".

# Note that "..." represents the consecutive values in between.

# Example:

# a) input = 5
#    output = 12345
# b) input = 8
#    output = 12345678


def consecutive_values(value):
    output = ""

    for i in range(1, value + 1):
        output = output + str(i)

    return output


# print(consecutive_values(5))
# print(consecutive_values(8))


def consecutive_values_int_only(value):
    output = value
    output_1 = 0
    output_2 = 0
    first = 10

    for i in range(value, 0, -1):
        if value >= 10:
            output_1 = output_1 + (first**(i + 1)) * (value - i)
        else:
            output_2 = output_2 + ((first**i) * (value - i))
    return (output + output_1 + output_2)

    # return output

print(consecutive_values_int_only(5))
print(consecutive_values_int_only(8))
print(consecutive_values_int_only(10))
