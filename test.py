# INTRODUCTION
# A palindrome is a word, number, phrase, or other sequence of characters which reads the same backward as forward, such as madam, racecar.

# PROBLEM 1
# Given a string of lowercase letters, determine if is a palindrome.
# If the word is a palindrome, return True.
# Otherwise, return false.

# Examples:
# input: banana
# output: false

# input: racecar
# output: true


def is_palindrome(word):
    is_palindrome = False
    for i in range((len(word) // 2)):
        if word[i] == word[-i - 1]:
            is_palindrome = True
        else:
            is_palindrome = False

    return is_palindrome

words = [
    "banana",
    "racecar"
]

for w in words:
    print(w, is_palindrome(w))

# PROBLEM 2
# Someone added a random character in our palindromes, create a function to determine the character
#  that need to be removed to make the word a palindrome again.
# There may be more than one solution, but any will do. For example, if your string is "bcbc",
#  you can either remove 'b' at index 0 or 'c' at index 3.
# If the word is already a palindrome, return -1.
# Otherwise, return the index of a character to remove.

# Examples
# input: aaab
# output: 3 // Removing 'b' at index 3 results in a palindrome

# input: baa
# output: 0 // Removing 'b' at index 0 results in a palindrome

# input: aaa
# output: -1 // This string is already a palindrome, so we print -1


def palindrome(word):
    is_palindrome = True

    for i in range((len(word) // 2)):
        if word[i] != word[-i - 1]:
            is_palindrome = False
            return i, is_palindrome
    return -1, is_palindrome


def remove_char_if_needed(word):
    index_w, is_palindrome = palindrome(word)

    if is_palindrome is True:
        return word, index_w

    found_palindrome = False
    while(found_palindrome is False):
        word_first = word.replace(word[index_w], "", 1)
        word_last = word.replace(word[-index_w - 1], "", 1)

        index_f, first = palindrome(word_first)
        index_o, last = palindrome(word_last)
        if first is True:
            return (word, index_w, word_first)
        elif last is True:
            return (word, -index_w - 1, word_last)


words = [
    "aaab",
    "baa",
    "aaa",
    "bcbc",
    "banana",
    "anbana",
    "anabna",
]

for w in words:
    print(remove_char_if_needed(w))
